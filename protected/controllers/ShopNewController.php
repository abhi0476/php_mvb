<?php

class ShopNewController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'view','getShopsJson'),
                'users' => array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'update'),
                'users' => array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete'),
                'users' => array('admin'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {

        //\Yii::log(print_r(ShopsController::getFilePath(ShopsController::getFilePath(ShopsController::getFilePath(ShopsController::getFilePath(dirname(__FILE__)))).'/uploadsmvb/'), true));
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $model = new Shops;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);



        if (isset($_POST['Shops'])) {
            $model->attributes = $_POST['Shops'];
            $model->starttime=date("H:i", strtotime($model->starttime));
            $model->endtime=date("H:i", strtotime($model->endtime));
            $model->save();
            $brand = new Brands();
            $brand->address = $model->address;
            $brand->mobile = $model->mobile;
            $brand->name = $model->name;
            $brand->save();

            $shopBrand = new ShopBrands();
            $shopBrand->shop_id = $model->id;
            $shopBrand->brand_id = $brand->id;
            $shopBrand->save();
            
            $catg=@$_POST['shopCategory'];
            if (is_array($catg)) {
                foreach ($catg as $selectedOption) {
                    $bcatgorie= new BrandCategories();
                    $bcatgorie->brand_id=$brand->id;
                    $bcatgorie->category_id=$selectedOption;
                    $bcatgorie->save();
                }
            } else {
                $bcatgorie= new BrandCategories();
                $bcatgorie->brand_id=$brand->id;
                $bcatgorie->category_id=$catg;
                $bcatgorie->save();
            }
            
            
            
        $shopImagePath = 'shop_';


        define("MAX_SIZE", "10000");
        $errors = 0;
        //\Yii::log(print_r('outside files', true));
        //$image=$_FILES['image1']['tmp_name'];
        if (isset($_FILES['image1'])&&!empty($_FILES['image1']['tmp_name'])) {
            // \Yii::log(print_r('inside files', true));
            $image = $_FILES['image1']['tmp_name'];
            $imagename = 'test';
            $shopImage = new ShopImages();
            $shopImage->shop_id = $model->id;
            $shopImage->image_url = 'shop_';
            $shopImage->image_name = @$_FILES['image1']['name'];
            $shopImage->insert();


            $imageId = $shopImage->id;
            //get the original name of the file from the clients machine
            $filename = stripslashes($_FILES['image1']['name']);
            //get the extension of the file in a lower case format
            $extension = ShopsController::getExtension($filename);
            $extension = strtolower($extension);
            //if it is not a known extension, we will suppose it is an error and will not  upload the file,  
            //otherwise we will do more tests
            if (($extension != "jpeg") && ($extension != "jpg") && ($extension != "gif")) {
                //print error message
                echo '<h1>Unknown extension!</h1>';
                $errors = 1;
            } else {

                $size = filesize($_FILES['image1']['tmp_name']);
                if ($size > MAX_SIZE * 1024) {
                    echo '<h1>You have exceeded the size limit!</h1>';
                    $errors = 1;
                }

                $image_name = $imageId . '.' . $extension;
                //echo 'basename '.basename(__DIR__);
                $newname = ShopsController::getFilePath(ShopsController::getFilePath(ShopsController::getFilePath(dirname(__FILE__)))) . '/uploadsmvb/' . $image_name;
                //\Yii::log(print_r($newname, true));
                $copied = copy($_FILES['image1']['tmp_name'], $newname);
                if (!$copied) {
                    echo '<h1>Copy unsuccessfull!</h1>';
                    $errors = 1;
                }
            }
        }
            
            
            $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $model = $this->loadModel($id);
        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);



        $shopImagePath = 'shop_';


        define("MAX_SIZE", "10000");
        $errors = 0;
        //\Yii::log(print_r('outside files', true));
        //$image=$_FILES['image1']['tmp_name'];
        if (isset($_FILES['image1'])&&!empty($_FILES['image1']['tmp_name'])) {
            // \Yii::log(print_r('inside files', true));
            $image = $_FILES['image1']['tmp_name'];
            $imagename = 'test';
            $shopImage = new ShopImages();
            $shopImage->shop_id = $model->id;
            $shopImage->image_url = 'shop_';
            $shopImage->image_name = @$_FILES['image1']['name'];
            $shopImage->insert();


            $imageId = $shopImage->id;
            //get the original name of the file from the clients machine
            $filename = stripslashes($_FILES['image1']['name']);
            //get the extension of the file in a lower case format
            $extension = ShopsController::getExtension($filename);
            $extension = strtolower($extension);
            //if it is not a known extension, we will suppose it is an error and will not  upload the file,  
            //otherwise we will do more tests
            if (($extension != "jpeg") && ($extension != "jpg") && ($extension != "gif")) {
                //print error message
                echo '<h1>Unknown extension!</h1>';
                $errors = 1;
            } else {

                $size = filesize($_FILES['image1']['tmp_name']);
                if ($size > MAX_SIZE * 1024) {
                    echo '<h1>You have exceeded the size limit!</h1>';
                    $errors = 1;
                }

                $image_name = $imageId . '.' . $extension;
                //echo 'basename '.basename(__DIR__);
                $newname = ShopsController::getFilePath(ShopsController::getFilePath(ShopsController::getFilePath(dirname(__FILE__)))) . '/uploadsmvb/' . $image_name;
                //\Yii::log(print_r($newname, true));
                $copied = copy($_FILES['image1']['tmp_name'], $newname);
                if (!$copied) {
                    echo '<h1>Copy unsuccessfull!</h1>';
                    $errors = 1;
                }
            }
        }
        if (isset($_POST['Shops'])) {
            $model->attributes = $_POST['Shops'];
            $model->starttime=date("H:i", strtotime($model->starttime));
            $model->endtime=date("H:i", strtotime($model->endtime));
            $shopbrands=$model->shopBrands;
            if(isset($shopbrands)&& count($shopbrands)>0){
                $brand_id=$shopbrands[0]->brand_id;
               \BrandCategories::model()->deleteAll('brand_id='.$brand_id);      
                 
            
            $catg=@$_POST['shopCategory'];
                if (is_array($catg)) {
                    foreach ($catg as $selectedOption) {
                        $bcatgorie= new BrandCategories();
                        $bcatgorie->brand_id=$brand_id;
                        $bcatgorie->category_id=$selectedOption;
                        $bcatgorie->save();
                    }
                } else {
                    $bcatgorie= new BrandCategories();
                    $bcatgorie->brand_id=$brand_id;
                    $bcatgorie->category_id=$catg;
                    $bcatgorie->save();
                }
            }
            if ($model->save()){
                $this->redirect(array('view', 'id' => $model->id));
            }
            
        }
        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $dataProvider = new CActiveDataProvider('Shops');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new Shops('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Shops']))
            $model->attributes = $_GET['Shops'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Shops the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = Shops::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Shops $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'shops-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public static function getExtension($str) {
        $i = strrpos($str, ".");
        if (!$i) {
            return "";
        }
        $l = strlen($str) - $i;
        $ext = substr($str, $i + 1, $l);

        return $ext;
    }

    public static function getFilePath($str) {
        $i = strrpos($str, "/");
        if (!$i) {
            return $str;
        }
        $l = strlen($str) - $i;
        $ext = substr($str, 0, $i);

        return $ext;
    }
    
    public function actionGetShopsJson(){
       
    }

}
