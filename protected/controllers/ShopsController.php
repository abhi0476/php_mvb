<?php

class ShopsController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'view','getShopsJson','getSearchShopsJson','getShopData','getCategory'),
                'users' => array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'update'),
                'users' => array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete'),
                'users' => array('admin'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {

        //\Yii::log(print_r(ShopsController::getFilePath(ShopsController::getFilePath(ShopsController::getFilePath(ShopsController::getFilePath(dirname(__FILE__)))).'/uploadsmvb/'), true));
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $model = new Shops;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);



        if (isset($_POST['Shops'])) {
            $model->attributes = $_POST['Shops'];
            $model->starttime=date("H:i", strtotime($model->starttime));
            $model->endtime=date("H:i", strtotime($model->endtime));
            $model->save();
            $brand = new Brands();
            $brand->address = $model->address;
            $brand->mobile = $model->mobile;
            $brand->name = $model->name;
            $brand->save();

            $shopBrand = new ShopBrands();
            $shopBrand->shop_id = $model->id;
            $shopBrand->brand_id = $brand->id;
            $shopBrand->save();
            
            $catg=@$_POST['shopCategory'];
            if (is_array($catg)) {
                foreach ($catg as $selectedOption) {
                    $bcatgorie= new BrandCategories();
                    $bcatgorie->brand_id=$brand->id;
                    $bcatgorie->category_id=$selectedOption;
                    $bcatgorie->save();

 $shopCategory = new ShopCategories();
                    $shopCategory->shop_id = $model->id;
                    $shopCategory->category_id = $selectedOption;
                    $shopCategory->save();
                }
            } else {
                $bcatgorie= new BrandCategories();
                $bcatgorie->brand_id=$brand->id;
                $bcatgorie->category_id=$catg;
                $bcatgorie->save();

$shopCategory = new ShopCategories();
                    $shopCategory->shop_id = $model->id;
                  $shopCategory->category_id = $catg;
                   $shopCategory->save(); 
            }
            
            
            
        $shopImagePath = 'shop_';


        define("MAX_SIZE", "10000");
        $errors = 0;
        //\Yii::log(print_r('outside files', true));
        //$image=$_FILES['image1']['tmp_name'];
        if (isset($_FILES['image1'])&&!empty($_FILES['image1']['tmp_name'])) {
            // \Yii::log(print_r('inside files', true));
            $image = $_FILES['image1']['tmp_name'];
            $imagename = 'test';
            $shopImage = new ShopImages();
            $shopImage->shop_id = $model->id;
            $shopImage->image_url = 'shop_';
            $shopImage->image_name = @$_FILES['image1']['name'];
            $shopImage->insert();


            $imageId = $shopImage->id;
            //get the original name of the file from the clients machine
            $filename = stripslashes($_FILES['image1']['name']);
            //get the extension of the file in a lower case format
            $extension = ShopsController::getExtension($filename);
            $extension = strtolower($extension);
            //if it is not a known extension, we will suppose it is an error and will not  upload the file,  
            //otherwise we will do more tests
            if (($extension != "jpeg") && ($extension != "jpg") && ($extension != "gif")) {
                //print error message
                echo '<h1>Unknown extension!</h1>';
                $errors = 1;
            } else {

                $size = filesize($_FILES['image1']['tmp_name']);
                if ($size > MAX_SIZE * 1024) {
                    echo '<h1>You have exceeded the size limit!</h1>';
                    $errors = 1;
                }

                $image_name = $imageId . '.' . $extension;
                //echo 'basename '.basename(__DIR__);
                $newname = ShopsController::getFilePath(ShopsController::getFilePath(ShopsController::getFilePath(dirname(__FILE__)))) . '/uploadsmvb/' . $image_name;
                //\Yii::log(print_r($newname, true));
                $copied = copy($_FILES['image1']['tmp_name'], $newname);
                if (!$copied) {
                    echo '<h1>Copy unsuccessfull!</h1>';
                    $errors = 1;
                }
            }
        }
            
            
            $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $model = $this->loadModel($id);
        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);



        $shopImagePath = 'shop_';


        define("MAX_SIZE", "10000");
        $errors = 0;
        //\Yii::log(print_r('outside files', true));
        //$image=$_FILES['image1']['tmp_name'];
        if (isset($_FILES['image1'])&&!empty($_FILES['image1']['tmp_name'])) {
            // \Yii::log(print_r('inside files', true));
            $image = $_FILES['image1']['tmp_name'];
            $imagename = 'test';
            $shopImage = new ShopImages();
            $shopImage->shop_id = $model->id;
            $shopImage->image_url = 'shop_';
            $shopImage->image_name = @$_FILES['image1']['name'];
            $shopImage->insert();


            $imageId = $shopImage->id;
            //get the original name of the file from the clients machine
            $filename = stripslashes($_FILES['image1']['name']);
            //get the extension of the file in a lower case format
            $extension = ShopsController::getExtension($filename);
            $extension = strtolower($extension);
            //if it is not a known extension, we will suppose it is an error and will not  upload the file,  
            //otherwise we will do more tests
            if (($extension != "jpeg") && ($extension != "jpg") && ($extension != "gif")) {
                //print error message
                echo '<h1>Unknown extension!</h1>';
                $errors = 1;
            } else {

                $size = filesize($_FILES['image1']['tmp_name']);
                if ($size > MAX_SIZE * 1024) {
                    echo '<h1>You have exceeded the size limit!</h1>';
                    $errors = 1;
                }

                $image_name = $imageId . '.' . $extension;
                //echo 'basename '.basename(__DIR__);
                $newname = ShopsController::getFilePath(ShopsController::getFilePath(ShopsController::getFilePath(dirname(__FILE__)))) . '/uploadsmvb/' . $image_name;
                //\Yii::log(print_r($newname, true));
                $copied = copy($_FILES['image1']['tmp_name'], $newname);
                if (!$copied) {
                    echo '<h1>Copy unsuccessfull!</h1>';
                    $errors = 1;
                }
            }
        }
        if (isset($_POST['Shops'])) {
            $model->attributes = $_POST['Shops'];
            $model->starttime=date("H:i", strtotime($model->starttime));
            $model->endtime=date("H:i", strtotime($model->endtime));
            $shopbrands=$model->shopBrands;
            if(isset($shopbrands)&& count($shopbrands)>0){
                $brand_id=$shopbrands[0]->brand_id;
               \BrandCategories::model()->deleteAll('brand_id='.$brand_id);      
$shopcategories=$model->shopCategories;
foreach ($shopcategories as $shopCategory) {
$category_id=$shopCategory->category_id;
                \ShopCategories::model()->deleteAll('category_id='.$category_id);  		
 }                  
            
            $catg=@$_POST['shopCategory'];
                if (is_array($catg)) {
                    foreach ($catg as $selectedOption) {
                        $bcatgorie= new BrandCategories();
                        $bcatgorie->brand_id=$brand_id;
                        $bcatgorie->category_id=$selectedOption;
                        $bcatgorie->save();
$shopCategory = new ShopCategories();
                    $shopCategory->shop_id = $model->id;
                    $shopCategory->category_id = $selectedOption;
                    $shopCategory->save();                    }
                } else {
                    $bcatgorie= new BrandCategories();
                    $bcatgorie->brand_id=$brand_id;
                    $bcatgorie->category_id=$catg;
                    $bcatgorie->save();
$shopCategory = new ShopCategories();
                    $shopCategory->shop_id = $model->id;
                    $shopCategory->category_id = $catg;
                    $shopCategory->save();                }
            }
            if ($model->save()){
                $this->redirect(array('view', 'id' => $model->id));
            }
            
        }
        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $dataProvider = new CActiveDataProvider('Shops');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new Shops('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Shops']))
            $model->attributes = $_GET['Shops'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Shops the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = Shops::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Shops $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'shops-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public static function getExtension($str) {
        $i = strrpos($str, ".");
        if (!$i) {
            return "";
        }
        $l = strlen($str) - $i;
        $ext = substr($str, $i + 1, $l);

        return $ext;
    }

    public static function getFilePath($str) {
        $i = strrpos($str, "/");
        if (!$i) {
            return $str;
        }
        $l = strlen($str) - $i;
        $ext = substr($str, 0, $i);

        return $ext;
    }
    
    public function actionGetShopsJson(){
        $lat=@$_GET['lat'];
        $lon=@$_GET['lon'];
        $dist=10;
        if(!empty($lat) && !empty($lon)){
            //$latDist = round(1.0 / 111.1 * 3.0,6);
            $lonDist = round(1.0 / abs(111.1110*cos((double)$lat)) * $dist,6);
            //$lonDist=$dist/abs(cos(deg2rad($lat))*111.05);
            $latDist =$dist/111.05;
            //$condition=" where  shops.latitude BETWEEN shops.latitude - ".$latDist." AND shops.latitude + ".$latDist." and shops.longitude BETWEEN shops.longitude - ".$lonDist." AND shops.longitude + ".$lonDist." ORDER BY ((shops.latitude-".$lat.")*(shops.latitude-".$lat.")) + ((shops.longitude-".$lon.")*(shops.longitude-".$lon.")) ";
            $condition=" where  shops.latitude BETWEEN shops.latitude - ".$latDist." AND shops.latitude + ".$latDist." and shops.longitude BETWEEN shops.longitude - ".$lonDist." AND shops.longitude + ".$lonDist;
            
            $distancestr=" 3956 * 2 * ASIN(SQRT( POWER(SIN((".$lat." - shops.latitude) *  pi()/180 / 2), 2) +COS(".$lat." * pi()/180) * COS(shops.latitude * pi()/180) * POWER(SIN((".$lon." -shops.longitude) * pi()/180 / 2), 2) ))
as distance  ";
            
         //   $ordercondition=" order by 3956 * 2 * ASIN(SQRT( POWER(SIN((".$lat." - shops.latitude) *  pi()/180 / 2), 2) +COS(".$lat." * pi()/180) * COS(shops.latitude * pi()/180) * POWER(SIN((".$lon." -shops.longitude) * pi()/180 / 2), 2) )) asc limit 15";
          
        //  $distcondition=" and 3956 * 2 * ASIN(SQRT( POWER(SIN((".$lat." - shops.latitude) *  pi()/180 / 2), 2) +COS(".$lat." * pi()/180) * COS(shops.latitude * pi()/180) * POWER(SIN((".$lon." -shops.longitude) * pi()/180 / 2), 2) )) <=$dist";
        $ordercondition=" order by distance asc limit 50";
            $distcondition=" having distance <=$dist";
            //$list= Yii::app()->db->createCommand("select id,name,address,latitude,longitude,mobile,anytimedelivery,starttime,endtime,charge,minimum_order,max_distance, (select group_concat(cat.name) from brand_categories as bc, categories as cat where bc.category_id=cat.id and bc.brand_id in( SELECT sb.brand_id FROM shop_brands as sb WHERE sb.shop_id=shops.id )) as catg,".$distancestr." from shops ".$condition.$distcondition.$ordercondition)->queryAll();
            $list= Yii::app()->db->createCommand("select id,name,address,latitude,longitude,mobile,anytimedelivery,starttime,endtime,charge,max_time, minimum_order,max_distance, (select group_concat(cat.name) from categories as cat where cat.id in( SELECT sc.category_id FROM shop_categories as sc WHERE sc.shop_id=shops.id )) as catg,".$distancestr." from shops ".$condition.$distcondition.$ordercondition)->queryAll();            
//print_r($list);
            $str= '{';
            $str.= '"shops":[';
            $i=0;
            $len=count($list);
            foreach($list as $v) {
                $i++;
                $catgoryarr=  explode(',',$v['catg']);
                if(isset($_GET['catg'])&& $_GET['catg']!='All'){
                    
                    if(!in_array($_GET['catg'], $catgoryarr)){
                        continue;
                    }
                }
                $str.= '{';
                $j=0;
                $l=count($v);
                foreach($v as $key =>$value) {
                    $j++;
                if($j!=$l){
                    $str.= '"'.$key.'":"'.$value.'",';}
                else{
                    $str.= '"'.$key.'":"'.$value.'"';
                }
               
                }
                if($i!=$len){
                $str.= '},';}
                else{
                    $str.= '}';
                }
            }
            $str.= ']';
            $str.= '}';
            echo $str;
            Yii::app()->end();
        }
    }

    public function actionGetSearchShopsJson(){
        $input=@$_GET['input'];
        $lat=@$_GET['lat'];
        $lon=@$_GET['lon'];
        $dist=40;
        if(!empty($input)){
             //$latDist = round(1.0 / 111.1 * 3.0,6);
            $lonDist = round(1.0 / abs(111.1110*cos((double)$lat)) * $dist,6);
            //$lonDist=$dist/abs(cos(deg2rad($lat))*111.05);
            $latDist =$dist/111.05;
            //$condition=" where  shops.latitude BETWEEN shops.latitude - ".$latDist." AND shops.latitude + ".$latDist." and shops.longitude BETWEEN shops.longitude - ".$lonDist." AND shops.longitude + ".$lonDist." ORDER BY ((shops.latitude-".$lat.")*(shops.latitude-".$lat.")) + ((shops.longitude-".$lon.")*(shops.longitude-".$lon.")) ";
            $condition=" where  shops.latitude BETWEEN shops.latitude - ".$latDist." AND shops.latitude + ".$latDist." and shops.longitude BETWEEN shops.longitude - ".$lonDist." AND shops.longitude + ".$lonDist;
            
            $distancestr=" 3956 * 2 * ASIN(SQRT( POWER(SIN((".$lat." - shops.latitude) *  pi()/180 / 2), 2) +COS(".$lat." * pi()/180) * COS(shops.latitude * pi()/180) * POWER(SIN((".$lon." -shops.longitude) * pi()/180 / 2), 2) ))
            as distance  ";
            
            //   $ordercondition=" order by 3956 * 2 * ASIN(SQRT( POWER(SIN((".$lat." - shops.latitude) *  pi()/180 / 2), 2) +COS(".$lat." * pi()/180) * COS(shops.latitude * pi()/180) * POWER(SIN((".$lon." -shops.longitude) * pi()/180 / 2), 2) )) asc limit 15";
          
            //  $distcondition=" and 3956 * 2 * ASIN(SQRT( POWER(SIN((".$lat." - shops.latitude) *  pi()/180 / 2), 2) +COS(".$lat." * pi()/180) * COS(shops.latitude * pi()/180) * POWER(SIN((".$lon." -shops.longitude) * pi()/180 / 2), 2) )) <=$dist";
            $ordercondition=" order by distance asc limit 5";
            $distcondition=" having distance <=$dist";
            
            $condition.=" and  shops.name like '%$input%' ";
            //$ordercondition="limit 5";
            //$list= Yii::app()->db->createCommand("select id,name,address,latitude,longitude,mobile,anytimedelivery,starttime,endtime,charge,max_time, minimum_order,max_distance, (select group_concat(cat.name) from brand_categories as bc, categories as cat where bc.category_id=cat.id and bc.brand_id in( SELECT sb.brand_id FROM shop_brands as sb WHERE sb.shop_id=shops.id )) as catg, ".$distancestr." from shops ".$condition.$distcondition.$ordercondition)->queryAll();
            $list= Yii::app()->db->createCommand("select id,name,address,latitude,longitude,mobile,anytimedelivery,starttime,endtime,charge,max_time, minimum_order,max_distance, (select group_concat(cat.name) from categories as cat where cat.id in( SELECT sc.category_id FROM shop_categories as sc WHERE sc.shop_id=shops.id )) as catg,".$distancestr." from shops ".$condition.$distcondition.$ordercondition)->queryAll();            
            //print_r($list);
            $str= '{';
            $str.= '"shops":[';
            $i=0;
            $len=count($list);
            foreach($list as $v) {
                $i++;
                
                $str.= '{';
                $j=0;
                $l=count($v);
                foreach($v as $key =>$value) {
                    $j++;
                if($j!=$l){
                    $str.= '"'.$key.'":"'.$value.'",';}
                else{
                    $str.= '"'.$key.'":"'.$value.'"';
                }
               
                }
                if($i!=$len){
                $str.= '},';}
                else{
                    $str.= '}';
                }
            }
            $str.= ']';
            $str.= '}';
            echo $str;
            Yii::app()->end();
        }
    }
    
    public function actionGetShopData(){
        $input=@$_GET['input'];
        $last=@$_GET['last'];
        $lat=@$_GET['lat'];
        $lon=@$_GET['lon'];
        $dist=50;
        if(!empty($input)){
            $condition=" where  shops.name like '%$input%' ";
            $ordercondition="limit 1";
            $distancestr=" 3956 * 2 * ASIN(SQRT( POWER(SIN((".$lat." - shops.latitude) *  pi()/180 / 2), 2) +COS(".$lat." * pi()/180) * COS(shops.latitude * pi()/180) * POWER(SIN((".$lon." -shops.longitude) * pi()/180 / 2), 2) ))
            as distance  ";
            //$list= Yii::app()->db->createCommand("select id,name,address,latitude,longitude,mobile,anytimedelivery,starttime,endtime,charge,max_time, minimum_order,max_distance, (select group_concat(cat.name) from brand_categories as bc, categories as cat where bc.category_id=cat.id and bc.brand_id in( SELECT sb.brand_id FROM shop_brands as sb WHERE sb.shop_id=shops.id )) as catg, ".$distancestr." from shops ".$condition.$ordercondition)->queryAll();
            $list= Yii::app()->db->createCommand("select id,name,address,latitude,longitude,mobile,anytimedelivery,starttime,endtime,charge,max_time, minimum_order,max_distance, (select group_concat(cat.name) from categories as cat where cat.id in( SELECT sc.category_id FROM shop_categories as sc WHERE sc.shop_id=shops.id )) as catg,".$distancestr." from shops ".$condition.$ordercondition)->queryAll();            

            $lonDist = round(1.0 / abs(111.1110*cos((double)$lat)) * $dist,6);
            $latDist =$dist/111.05;
            $condition=" where  shops.latitude BETWEEN shops.latitude - ".$latDist." AND shops.latitude + ".$latDist." and shops.longitude BETWEEN shops.longitude - ".$lonDist." AND shops.longitude + ".$lonDist;
            
            $ordercondition=" order by distance asc limit 4";
            $distcondition=" having distance <=$dist";
            $condition.=" and  shops.name like '%$last%'  and shops.id !=".$list[0]['id'];
            //$listlast= Yii::app()->db->createCommand("select id,name,address,latitude,longitude,mobile,anytimedelivery,starttime,endtime,charge,max_time, minimum_order,max_distance, (select group_concat(cat.name) from brand_categories as bc, categories as cat where bc.category_id=cat.id and bc.brand_id in( SELECT sb.brand_id FROM shop_brands as sb WHERE sb.shop_id=shops.id )) as catg, ".$distancestr." from shops ".$condition.$distcondition.$ordercondition)->queryAll();
            $listlast= Yii::app()->db->createCommand("select id,name,address,latitude,longitude,mobile,anytimedelivery,starttime,endtime,charge,max_time, minimum_order,max_distance, (select group_concat(cat.name) from categories as cat where cat.id in( SELECT sc.category_id FROM shop_categories as sc WHERE sc.shop_id=shops.id )) as catg,".$distancestr." from shops ".$condition.$distcondition.$ordercondition)->queryAll();            

            $str= '{';
            $str.= '"shops":[';
            $i=0;
            $len=count($list);
            foreach($list as $v) {
                $i++;
               
                $str.= '{';
                $j=0;
                $l=count($v);
                foreach($v as $key =>$value) {
                    $j++;
                if($j!=$l){
                    $str.= '"'.$key.'":"'.$value.'",';}
                else{
                    $str.= '"'.$key.'":"'.$value.'"';
                }
               
                }
                if($i!=$len){
                $str.= '},';}
                else{
                    $str.= '}';
                }
            }
            
            
            $i=0;
            $len=count($listlast);
            if($len>0){
                $str.= ',';
            }
            foreach($listlast as $v) {
                $i++;
               
                $str.= '{';
                $j=0;
                $l=count($v);
                foreach($v as $key =>$value) {
                    $j++;
                if($j!=$l){
                    $str.= '"'.$key.'":"'.$value.'",';}
                else{
                    $str.= '"'.$key.'":"'.$value.'"';
                }
               
                }
                if($i!=$len){
                $str.= '},';}
                else{
                    $str.= '}';
                }
            }
            
            
            $str.= ']';
            $str.= '}';
            echo $str;
            Yii::app()->end();
        }
    }
    
    public function actionGetCategory(){
       
            $list= Yii::app()->db->createCommand("select id,name from categories ")->queryAll();
            //print_r($list);
            $str= '{';
            $str.= '"category":[';
            $i=0;
            $len=count($list);
            foreach($list as $v) {
                $i++;
                
                $str.= '{';
                $j=0;
                $l=count($v);
                foreach($v as $key =>$value) {
                    $j++;
                if($j!=$l){
                    $str.= '"'.$key.'":"'.$value.'",';}
                else{
                    $str.= '"'.$key.'":"'.$value.'"';
                }
               
                }
                if($i!=$len){
                $str.= '},';}
                else{
                    $str.= '}';
                }
            }
            $str.= ']';
            $str.= '}';
            echo $str;
            Yii::app()->end();
        
    }
}
