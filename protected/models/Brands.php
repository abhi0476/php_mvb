<?php

/**
 * This is the model class for table "brands".
 *
 * The followings are the available columns in table 'brands':
 * @property integer $id
 * @property string $name
 * @property string $address
 * @property string $mobile
 * @property string $created
 * @property integer $enabled
 *
 * The followings are the available model relations:
 * @property BrandCategories[] $brandCategories
 * @property BrandImages[] $brandImages
 * @property BrandProducts[] $brandProducts
 * @property ShopBrands[] $shopBrands
 */
class Brands extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'brands';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, address, mobile', 'required'),
			array('enabled', 'numerical', 'integerOnly'=>true),
			array('mobile', 'length', 'max'=>200),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
                    array('id, name, address, mobile, created, enabled', 'safe'),
			array('id, name, address, mobile, created, enabled', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			//'brandCategories' => array(self::HAS_MANY, 'BrandCategories', 'brand_id'),
                        'brandCategories' => array(self::MANY_MANY, 'Category','brand_categories(brand_id, category_id)'),
			'brandImages' => array(self::HAS_MANY, 'BrandImages', 'brand_id'),
			//'brandProducts' => array(self::HAS_MANY, 'BrandProducts', 'brand_id'),
                        'brandProducts' => array(self::MANY_MANY, 'BrandProducts','brand_products(brand_id, product_id)'),
			//'shopBrands' => array(self::HAS_MANY, 'ShopBrands', 'brand_id'),
                        'shopBrands' => array(self::MANY_MANY, 'ShopBrands','shop_brands(brand_id, shop_id)'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'address' => 'Address',
			'mobile' => 'Mobile',
			'created' => 'Created',
			'enabled' => 'Enabled',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('address',$this->address,true);
		$criteria->compare('mobile',$this->mobile,true);
		$criteria->compare('created',$this->created,true);
		$criteria->compare('enabled',$this->enabled);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Brands the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
