<?php

/**
 * This is the model class for table "brand_products".
 *
 * The followings are the available columns in table 'brand_products':
 * @property integer $id
 * @property integer $brand_id
 * @property integer $product_id
 * @property integer $enabled
 * @property double $price
 * @property double $discount
 * @property double $latitude
 * @property double $longitude
 * @property double $distance
 *
 * The followings are the available model relations:
 * @property Brands $brand
 * @property Products $product
 */
class BrandProducts extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'brand_products';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('brand_id, product_id, enabled, price, discount, latitude, longitude, distance', 'required'),
			array('brand_id, product_id, enabled', 'numerical', 'integerOnly'=>true),
			array('price, discount, latitude, longitude, distance', 'numerical'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
                    array('id, brand_id, product_id, enabled, price, discount, latitude, longitude, distance', 'safe'),
			array('id, brand_id, product_id, enabled, price, discount, latitude, longitude, distance', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'brand' => array(self::BELONGS_TO, 'Brands', 'brand_id'),
			'product' => array(self::BELONGS_TO, 'Products', 'product_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'brand_id' => 'Brand',
			'product_id' => 'Product',
			'enabled' => 'Enabled',
			'price' => 'Price',
			'discount' => 'Discount',
			'latitude' => 'Latitude',
			'longitude' => 'Longitude',
			'distance' => 'Distance',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('brand_id',$this->brand_id);
		$criteria->compare('product_id',$this->product_id);
		$criteria->compare('enabled',$this->enabled);
		$criteria->compare('price',$this->price);
		$criteria->compare('discount',$this->discount);
		$criteria->compare('latitude',$this->latitude);
		$criteria->compare('longitude',$this->longitude);
		$criteria->compare('distance',$this->distance);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return BrandProducts the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
