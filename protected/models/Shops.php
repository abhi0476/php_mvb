<?php

/**
 * This is the model class for table "shops".
 *
 * The followings are the available columns in table 'shops':
 * @property integer $id
 * @property string $name
 * @property double $latitude
 * @property double $longitude
 * @property string $address
 * @property string $mobile
 * @property string $details
 * @property integer $enabled
 * @property string $created
 * @property integer $anytimedelivery
 * @property string $starttime
 * @property string $endtime
 * @property integer $charge
 * @property integer $minimum_order
 * @property double $max_distance
 * @property integer $max_time
 * @property integer $country
 * @property integer $state
 * @property integer $city
 * @property string $pincode
 *
 * The followings are the available model relations:
 * @property ShopBrands[] $shopBrands
 * @property ShopImages[] $shopImages
 * @property ShopReviews[] $shopReviews
 */
class Shops extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'shops';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, latitude, longitude, address, mobile, starttime, endtime', 'required'),
			array('enabled, anytimedelivery, charge, minimum_order, max_time, country, state, city', 'numerical', 'integerOnly'=>true),
			array('latitude, longitude, max_distance', 'numerical'),
			array('mobile', 'length', 'max'=>200),
			array('pincode', 'length', 'max'=>10),
                    array('id, name, latitude, longitude, address, mobile, details, enabled, created, anytimedelivery, starttime, endtime, charge, minimum_order, max_distance, max_time, country, state, city, pincode', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name, latitude, longitude, address, mobile, details, enabled, created, anytimedelivery, starttime, endtime, charge, minimum_order, max_distance, max_time, country, state, city, pincode', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'shopBrands' => array(self::HAS_MANY, 'ShopBrands', 'shop_id'),
			'shopCategories' => array(self::HAS_MANY, 'ShopCategories', 'shop_id'),
			'shopImages' => array(self::HAS_MANY, 'ShopImages', 'shop_id'),
			'shopReviews' => array(self::HAS_MANY, 'ShopReviews', 'shop_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'latitude' => 'Latitude',
			'longitude' => 'Longitude',
			'address' => 'Address',
			'mobile' => 'Mobile',
			'details' => 'Details',
			'enabled' => 'Enabled',
			'created' => 'Created',
			'anytimedelivery' => 'Anytimedelivery',
			'starttime' => 'Starttime',
			'endtime' => 'Endtime',
			'charge' => 'Charge',
			'minimum_order' => 'Minimum Order',
			'max_distance' => 'Max Distance',
			'max_time' => 'Max Time',
			'country' => 'Country',
			'state' => 'State',
			'city' => 'City',
			'pincode' => 'Pincode',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('latitude',$this->latitude);
		$criteria->compare('longitude',$this->longitude);
		$criteria->compare('address',$this->address,true);
		$criteria->compare('mobile',$this->mobile,true);
		$criteria->compare('details',$this->details,true);
		$criteria->compare('enabled',$this->enabled);
		$criteria->compare('created',$this->created,true);
		$criteria->compare('anytimedelivery',$this->anytimedelivery);
		$criteria->compare('starttime',$this->starttime,true);
		$criteria->compare('endtime',$this->endtime,true);
		$criteria->compare('charge',$this->charge);
		$criteria->compare('minimum_order',$this->minimum_order);
		$criteria->compare('max_distance',$this->max_distance);
		$criteria->compare('max_time',$this->max_time);
		$criteria->compare('country',$this->country);
		$criteria->compare('state',$this->state);
		$criteria->compare('city',$this->city);
		$criteria->compare('pincode',$this->pincode,true);
                $criteria->order = 'id  DESC';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Shops the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
        public function getImagesLink(){
            $str='';
            $str.="";
                $i=1;
                foreach($this->shopImages as $image){
                    $str.'<a href="http://gpl4you.com/uploadsmvb/'.$image->id.'.jpg" target="_blank" style="padding-right:10px">Image '.$i++.'</a>';
                }
            return $str;
        }
}
