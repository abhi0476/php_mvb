<?php
/* @var $this BrandProductsController */
/* @var $model BrandProducts */

$this->breadcrumbs=array(
	'Brand Products'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List BrandProducts', 'url'=>array('index')),
	array('label'=>'Create BrandProducts', 'url'=>array('create')),
	array('label'=>'View BrandProducts', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage BrandProducts', 'url'=>array('admin')),
);
?>

<h1>Update BrandProducts <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>