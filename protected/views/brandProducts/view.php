<?php
/* @var $this BrandProductsController */
/* @var $model BrandProducts */

$this->breadcrumbs=array(
	'Brand Products'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List BrandProducts', 'url'=>array('index')),
	array('label'=>'Create BrandProducts', 'url'=>array('create')),
	array('label'=>'Update BrandProducts', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete BrandProducts', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage BrandProducts', 'url'=>array('admin')),
);
?>

<h1>View BrandProducts #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'brand_id',
		'product_id',
		'enabled',
		'price',
		'discount',
		'latitude',
		'longitude',
		'distance',
	),
)); ?>
