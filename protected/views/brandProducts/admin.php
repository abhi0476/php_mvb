<?php
/* @var $this BrandProductsController */
/* @var $model BrandProducts */

$this->breadcrumbs=array(
	'Brand Products'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List BrandProducts', 'url'=>array('index')),
	array('label'=>'Create BrandProducts', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#brand-products-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Brand Products</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'brand-products-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
            array(
            'name' => 'brand_id',
            'value' => '$data->brand->name',
          //  'filter' => \CartStatus::$cartStatusMap,
            'headerHtmlOptions' => array('style' => 'min-width: 100px;'),
            'type' => 'raw'
            ),
		'brand_id',
            array(
            'name' => 'product_id',
            'value' => '$data->product->name',
          //  'filter' => \CartStatus::$cartStatusMap,
            'headerHtmlOptions' => array('style' => 'min-width: 100px;'),
            'type' => 'raw'
            ),
		'product_id',
		'enabled',
		'price',
		'discount',
		/*
		'latitude',
		'longitude',
		'distance',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
