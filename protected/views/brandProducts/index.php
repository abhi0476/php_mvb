<?php
/* @var $this BrandProductsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Brand Products',
);

$this->menu=array(
	array('label'=>'Create BrandProducts', 'url'=>array('create')),
	array('label'=>'Manage BrandProducts', 'url'=>array('admin')),
);
?>

<h1>Brand Products</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
