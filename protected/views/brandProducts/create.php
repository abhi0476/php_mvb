<?php
/* @var $this BrandProductsController */
/* @var $model BrandProducts */

$this->breadcrumbs=array(
	'Brand Products'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List BrandProducts', 'url'=>array('index')),
	array('label'=>'Manage BrandProducts', 'url'=>array('admin')),
);
?>

<h1>Create BrandProducts</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>