<?php
/* @var $this ShopBrandsController */
/* @var $model ShopBrands */

$this->breadcrumbs=array(
	'Shop Brands'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List ShopBrands', 'url'=>array('index')),
	array('label'=>'Create ShopBrands', 'url'=>array('create')),
	array('label'=>'Update ShopBrands', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete ShopBrands', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage ShopBrands', 'url'=>array('admin')),
);
?>

<h1>View ShopBrands #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'shop_id',
		'brand_id',
		'enabled',
	),
)); ?>
