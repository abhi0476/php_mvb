<?php
/* @var $this ShopBrandsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Shop Brands',
);

$this->menu=array(
	array('label'=>'Create ShopBrands', 'url'=>array('create')),
	array('label'=>'Manage ShopBrands', 'url'=>array('admin')),
);
?>

<h1>Shop Brands</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
