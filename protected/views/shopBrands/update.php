<?php
/* @var $this ShopBrandsController */
/* @var $model ShopBrands */

$this->breadcrumbs=array(
	'Shop Brands'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List ShopBrands', 'url'=>array('index')),
	array('label'=>'Create ShopBrands', 'url'=>array('create')),
	array('label'=>'View ShopBrands', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage ShopBrands', 'url'=>array('admin')),
);
?>

<h1>Update ShopBrands <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>