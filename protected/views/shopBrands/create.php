<?php
/* @var $this ShopBrandsController */
/* @var $model ShopBrands */

$this->breadcrumbs=array(
	'Shop Brands'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List ShopBrands', 'url'=>array('index')),
	array('label'=>'Manage ShopBrands', 'url'=>array('admin')),
);
?>

<h1>Create ShopBrands</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>