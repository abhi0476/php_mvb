<?php
/* @var $this ShopBrandsController */
/* @var $model ShopBrands */

$this->breadcrumbs=array(
	'Call Logs'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List ShopBrands', 'url'=>array('index')),
	array('label'=>'Delete ShopBrands', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage ShopBrands', 'url'=>array('admin')),
);
?>

<h1>View CallLos #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'shop_id',
		'phone_no',
		'device_id',
		'latitude',
		'longitude'
	),
)); ?>
