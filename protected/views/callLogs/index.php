<?php
/* @var $this ShopBrandsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Call Logs',
);

$this->menu=array(
	array('label'=>'Manage ShopBrands', 'url'=>array('admin')),
);
?>

<h1>Call Logs</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
