<?php
/* @var $this ShopBrandsController */
/* @var $model ShopBrands */

$this->breadcrumbs=array(
	'Call Logs'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List CallLogs', 'url'=>array('index')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#call-logs-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Call Logs</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'call-logs-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		array(
            'name' => 'shop_id',
            'value' => '$data->shop->name',
          //  'filter' => \CartStatus::$cartStatusMap,
            'headerHtmlOptions' => array('style' => 'min-width: 100px;'),
            'type' => 'raw'
            ),
		'shop_id',
		'phone_no',
		'device_id',
		'latitude',
		'longitude',
		'email_id',
		'duration',
		array(
            'name' => 'date_called',
            'headerHtmlOptions' => array('style' => 'min-width: 100px;'),
            'type' => 'raw'
            ),
		array(
			'class'=>'CButtonColumn',
			'template'=>'{view}',
		),
	),
)); ?>
