<?php
/* @var $this ShopReviewsController */
/* @var $model ShopReviews */

$this->breadcrumbs=array(
	'Shop Reviews'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List ShopReviews', 'url'=>array('index')),
	array('label'=>'Create ShopReviews', 'url'=>array('create')),
	array('label'=>'View ShopReviews', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage ShopReviews', 'url'=>array('admin')),
);
?>

<h1>Update ShopReviews <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>