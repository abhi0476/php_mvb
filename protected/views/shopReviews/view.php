<?php
/* @var $this ShopReviewsController */
/* @var $model ShopReviews */

$this->breadcrumbs=array(
	'Shop Reviews'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List ShopReviews', 'url'=>array('index')),
	array('label'=>'Create ShopReviews', 'url'=>array('create')),
	array('label'=>'Update ShopReviews', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete ShopReviews', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage ShopReviews', 'url'=>array('admin')),
);
?>

<h1>View ShopReviews #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'review',
		'user_id',
		'created',
		'enabled',
		'shop_id',
	),
)); ?>
