<?php
/* @var $this ShopReviewsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Shop Reviews',
);

$this->menu=array(
	array('label'=>'Create ShopReviews', 'url'=>array('create')),
	array('label'=>'Manage ShopReviews', 'url'=>array('admin')),
);
?>

<h1>Shop Reviews</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
