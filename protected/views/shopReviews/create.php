<?php
/* @var $this ShopReviewsController */
/* @var $model ShopReviews */

$this->breadcrumbs=array(
	'Shop Reviews'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List ShopReviews', 'url'=>array('index')),
	array('label'=>'Manage ShopReviews', 'url'=>array('admin')),
);
?>

<h1>Create ShopReviews</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>