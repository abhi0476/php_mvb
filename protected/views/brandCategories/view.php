<?php
/* @var $this BrandCategoriesController */
/* @var $model BrandCategories */

$this->breadcrumbs=array(
	'Brand Categories'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List BrandCategories', 'url'=>array('index')),
	array('label'=>'Create BrandCategories', 'url'=>array('create')),
	array('label'=>'Update BrandCategories', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete BrandCategories', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage BrandCategories', 'url'=>array('admin')),
);
?>

<h1>View BrandCategories #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'brand_id',
		'category_id',
		'enabled',
	),
)); ?>
