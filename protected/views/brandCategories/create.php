<?php
/* @var $this BrandCategoriesController */
/* @var $model BrandCategories */

$this->breadcrumbs=array(
	'Brand Categories'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List BrandCategories', 'url'=>array('index')),
	array('label'=>'Manage BrandCategories', 'url'=>array('admin')),
);
?>

<h1>Create BrandCategories</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>