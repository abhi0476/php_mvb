<?php
/* @var $this BrandCategoriesController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Brand Categories',
);

$this->menu=array(
	array('label'=>'Create BrandCategories', 'url'=>array('create')),
	array('label'=>'Manage BrandCategories', 'url'=>array('admin')),
);
?>

<h1>Brand Categories</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
