<?php
/* @var $this BrandCategoriesController */
/* @var $model BrandCategories */

$this->breadcrumbs=array(
	'Brand Categories'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List BrandCategories', 'url'=>array('index')),
	array('label'=>'Create BrandCategories', 'url'=>array('create')),
	array('label'=>'View BrandCategories', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage BrandCategories', 'url'=>array('admin')),
);
?>

<h1>Update BrandCategories <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>