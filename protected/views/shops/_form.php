<?php
/* @var $this ShopsController */
/* @var $model Shops */
/* @var $form CActiveForm */
?>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.4/jquery.min.js"></script>
<link rel="stylesheet" type="text/css" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.21/themes/redmond/jquery-ui.css" />
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<link rel="stylesheet" href="/css/jquery.ptTimeSelect.css" type="text/css" media="screen">
<script src="/js/jquery.ptTimeSelect.js"></script>
<div class="form">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'shops-form',
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // There is a call to performAjaxValidation() commented in generated controller code.
        // See class documentation of CActiveForm for details on this.
        'enableAjaxValidation' => false,
    ));
    ?>

    <p class="note">Fields with <span class="required">*</span> are required.</p>

        <?php echo $form->errorSummary($model); ?>

    <div class="row">
        <?php echo $form->labelEx($model, 'name'); ?>
<?php echo $form->textArea($model, 'name', array('rows' => 6, 'cols' => 50)); ?>
<?php echo $form->error($model, 'name'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'latitude'); ?>
<?php echo $form->textField($model, 'latitude'); ?>
<?php echo $form->error($model, 'latitude'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'longitude'); ?>
<?php echo $form->textField($model, 'longitude'); ?>
<?php echo $form->error($model, 'longitude'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'address'); ?>
<?php echo $form->textArea($model, 'address', array('rows' => 6, 'cols' => 50)); ?>
<?php echo $form->error($model, 'address'); ?>
    </div>

    <div class="row selDiv" >
        <select id="shopCategory" name="shopCategory[]" multiple="multiple" style="height: 270px;width: 200px;">
            <?php
            $categories = \Categories::model()->findAll();
            if (!$model->isNewRecord) {
                $shopbrands = $model->shopBrands;

                if (count($shopbrands) > 0) {
                    $brand_id = $shopbrands[0]->brand_id;
                    $avlCategories = \BrandCategories::model()->findAll('brand_id='.$brand_id); 
                }
            }
            $flag = false;
            foreach ($categories as $category) {
                $flag = false;
                if(isset($avlCategories) && count($avlCategories)>0){
                         
                    foreach($avlCategories as $value){
                        if(isset($value->category_id) && $value->category_id==$category->id){
                            $flag=true;
                        }
                    }}
                ?>

                <option  value="<?php echo $category->id; ?>"  <?php if ($flag) {
                    echo "selected='selected'";
                } ?>><?php echo $category->name; ?></option>

        <?php } ?>
        </select>
    </div>
    <div class="row">
<?php echo $form->labelEx($model, 'mobile'); ?>
<?php echo $form->textField($model, 'mobile', array('size' => 15, 'maxlength' => 200)); ?>
        <?php echo $form->error($model, 'mobile'); ?>
    </div>

    <div class="row">
<?php echo $form->labelEx($model, 'details'); ?>
<?php echo $form->textArea($model, 'details', array('rows' => 6, 'cols' => 50)); ?>
        <?php echo $form->error($model, 'details'); ?>
    </div>

    <div class="row">
<?php echo $form->labelEx($model, 'enabled'); ?>
<?php echo $form->textField($model, 'enabled'); ?>
<?php echo $form->error($model, 'enabled'); ?>
    </div>


    <div class="row">
<?php echo $form->labelEx($model, 'anytimedelivery'); ?>
<?php echo $form->textField($model, 'anytimedelivery'); ?>
        <?php echo $form->error($model, 'anytimedelivery'); ?>
    </div>

    <div class="row">
<?php echo $form->labelEx($model, 'starttime'); ?>
<?php echo $form->textField($model, 'starttime'); ?>
        <?php echo $form->error($model, 'starttime'); ?>
    </div>

    <div class="row">
<?php echo $form->labelEx($model, 'endtime'); ?>
<?php echo $form->textField($model, 'endtime'); ?>
        <?php echo $form->error($model, 'endtime'); ?>
    </div>

    <div class="row">
<?php echo $form->labelEx($model, 'charge'); ?>
<?php echo $form->textField($model, 'charge'); ?>
        <?php echo $form->error($model, 'charge'); ?>
    </div>

    <div class="row">
<?php echo $form->labelEx($model, 'minimum_order'); ?>
<?php echo $form->textField($model, 'minimum_order'); ?>
        <?php echo $form->error($model, 'minimum_order'); ?>
    </div>

    <div class="row">
<?php echo $form->labelEx($model, 'max_distance'); ?>
<?php echo $form->textField($model, 'max_distance'); ?>
        <?php echo $form->error($model, 'max_distance'); ?>
    </div>

    <div class="row">
<?php echo $form->labelEx($model, 'max_time'); ?>
<?php echo $form->textField($model, 'max_time'); ?>
        <?php echo $form->error($model, 'max_time'); ?>
    </div>

    <div class="row">
<?php echo $form->labelEx($model, 'country'); ?>
<?php echo $form->textField($model, 'country'); ?>
        <?php echo $form->error($model, 'country'); ?>
    </div>

    <div class="row">
<?php echo $form->labelEx($model, 'state'); ?>
<?php echo $form->textField($model, 'state'); ?>
        <?php echo $form->error($model, 'state'); ?>
    </div>

    <div class="row">
<?php echo $form->labelEx($model, 'city'); ?>
<?php echo $form->textField($model, 'city'); ?>
        <?php echo $form->error($model, 'city'); ?>
    </div>

    <div class="row">
<?php echo $form->labelEx($model, 'pincode'); ?>
        <?php echo $form->textField($model, 'pincode', array('size' => 10, 'maxlength' => 10)); ?>
        <?php echo $form->error($model, 'pincode'); ?>
    </div>
    <div class="row">
        <?php
        echo "<table><tr><td>Images</td></tr>";
        $i = 1;
        foreach ($model->shopImages as $image) {
            echo '<tr><td><a href="http://gpl4you.com/uploadsmvb/' . $image->id . '.jpg" target="_blank">Image ' . $i++ . '</a></td></tr>';
        }

        echo "</table>";
        ?>
    </div>
    <div>
        <table>
            <tr>
                <td>
                    <span>Image Upload</span>
                    <input type="file"  name="image1" id="image1"></td>
            </tr></table>
    </div>
    <div class="row buttons">
    <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
    </div>

<?php $this->endWidget(); ?>

</div><!-- form -->
<script>
    $(document).ready(function () {
        $('input[name="Shops[starttime]"]').ptTimeSelect();
        $('input[name="Shops[endtime]"]').ptTimeSelect();

//                $( "select" ).change( displayVals );
//                function displayVals() {
//                    
//                    var multipleValues = $( "#shopCategory" ).val() || [];
//                    alert(multipleValues.join(','));
//                    $('.selDiv option').removeAttr('selected');
//                    for (index = 0; index < multipleValues.length; ++index) {
//                         value=multipleValues[index];
//                         //alert($('.selDiv option:eq('+value+')').val());
//                        //$('.selDiv option:eq('+value+')').prop('selected', true);
//                        $('.selDiv option').filter(function(i, e) { return $(e).val() == value}).prop('selected', 'selected');
//                    }
//                  }


    });
</script>