<?php
/* @var $this ShopsController */
/* @var $model Shops */

$this->breadcrumbs=array(
	'Shops'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List Shops', 'url'=>array('index')),
	array('label'=>'Create Shops', 'url'=>array('create')),
	array('label'=>'Update Shops', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Shops', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Shops', 'url'=>array('admin')),
);
?>

<h1>View Shops #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
		'latitude',
		'longitude',
		'address',
		'mobile',
		'details',
		'enabled',
		'created',
		'anytimedelivery',
		'starttime',
		'endtime',
		'charge',
		'minimum_order',
		'max_distance',
		'max_time',
		'country',
		'state',
		'city',
		'pincode',
	),
)); 

echo "<table><tr><td>Images</td></tr>";
$i=1;
foreach($model->shopImages as $image){
    echo '<tr><td><a href="http://gpl4you.com/uploadsmvb/'.$image->id.'.jpg" target="_blank">Image '.$i++.'</a></td></tr>';
}

echo "</table>";
?>
