<?php
/* @var $this ShopsController */
/* @var $model Shops */

$this->breadcrumbs=array(
	'Shops'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Shops', 'url'=>array('index')),
	array('label'=>'Create Shops', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#shops-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Shops</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'shops-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'name',
		'latitude',
		'longitude',
		'address',
		'mobile',
             array(
                'header' => 'Images',
                'value' => function($data ) {
                    $str='';
                    $str.="";
                        $i=1;
                        foreach($data->shopImages as $image){
                            $str.='<a href="http://gpl4you.com/uploadsmvb/'.$image->id.'.jpg" target="_blank" style="padding-right:10px">Image_'.$i++.'</a>';
                        }
                    return $str;
                },
                'type' => 'raw'
                ),
		/*
		'details',
		'enabled',
		'created',
		'anytimedelivery',
		'starttime',
		'endtime',
		'charge',
		'minimum_order',
		'max_distance',
		'max_time',
		'country',
		'state',
		'city',
		'pincode',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
